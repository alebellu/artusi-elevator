/* LICENSE START */
/*
 * ssoup
 * https://github.com/alebellu/ssoup
 *
 * Copyright 2012 Alessandro Bellucci
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://alebellu.github.com/licenses/GPL-LICENSE.txt
 * http://alebellu.github.com/licenses/MIT-LICENSE.txt
 */
/* LICENSE END */

/* HEADER START */
var env, requirejs, define;
if (typeof module !== 'undefined' && module.exports) {
    env = "node";
    requirejs = require('requirejs');
    define = requirejs.define;

    requirejs.config({
        baseUrl: __dirname,
        nodeRequire: require
    });

    // require all dependant libraries, so they will be available for the client to request.
    var pjson = require('./package.json');
    for (var library in pjson.dependencies) {
        require(library);
    }
} else {
    env = "browser";
    //requirejs = require;
}

({ define: env === "browser"
        ? define
        : function(A,F) { // nodejs.
            var path = require('path');
            var moduleName = path.basename(__filename, '.js');
            console.log("Loading module " + moduleName);
            module.exports = F.apply(null, A.map(require));
            global.registerUrlContext("/" + moduleName + ".js", {
                "static": __dirname,
                defaultPage: moduleName + ".js"
            });
            global.registerUrlContext("/" + moduleName, {
                "static": __dirname
            });
        }
}).
/* HEADER END */
define(['jquery', 'artusi-kitchen-tools'], function($, tools) {

    var drdf = tools.drdf;
    var drff = tools.drff;

    var elevatorType = function(context, options) {
        var elevator = this;
        elevator.context = context;
        elevator.initOptions = options;
        elevator.declaredAttributes = elevator.initOptions.declaredAttributes;
        elevator.conf = elevator.initOptions.conf;
    };

    elevatorType.loadElevator = function(context, options) {
        var dr = $.Deferred();

        // load the elevator driver code
        var elevatorConf = options.elevatorConf;
        require([elevatorConf["se:driver"]], function(specializedElevatorType) {
            var elevator = new elevatorType(context, {
                declaredAttributes: elevatorConf["se:attributes"],
                conf: elevatorConf["se:conf"]
            });

            var specializedElevator = new specializedElevatorType(context);
            $.extend(elevator, specializedElevator);

            elevator.init().done(function() {
                dr.resolve(elevator);
            }).fail(drff(dr));
        });

        return dr.promise();
    };

    elevatorType.prototype.getIRI = function() {
        return this.conf["se:iri"];
    };

    elevatorType.prototype.getAttributes = function() {
        var attributes = {
            "sw:Elevator": true
        };
        if (this.getSpecificAttributes) {
            $.extend(attributes, this.getSpecificAttributes(), attributes);
        }
        if (this.declaredAttributes) {
            $.extend(attributes, this.declaredAttributes, attributes);
        }
        return attributes;
    };

    /**
     * @param options
     *   @message the message to be processed.
     *   @recipeients the recipients of the message
     */
    elevatorType.prototype.onMessage = function(options) {
        var elevator = this;
        var dr = $.Deferred();
        var msg = options.message;

        elevator.deliverMessage(options).done(function(results) {
            if (results && $.isArray(results) && results.length > 0) {
                dr.resolve(results);
            } else {
                dr.resolve();
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    return elevatorType;
});
